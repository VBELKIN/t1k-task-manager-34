package ru.t1k.vbelkin.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.api.endpoint.IAuthEndpoint;
import ru.t1k.vbelkin.tm.api.service.IAuthService;
import ru.t1k.vbelkin.tm.api.service.IServiceLocator;
import ru.t1k.vbelkin.tm.dto.request.UserLoginRequest;
import ru.t1k.vbelkin.tm.dto.request.UserLogoutRequest;
import ru.t1k.vbelkin.tm.dto.request.UserProfileRequest;
import ru.t1k.vbelkin.tm.dto.response.UserLoginResponse;
import ru.t1k.vbelkin.tm.dto.response.UserLogoutResponse;
import ru.t1k.vbelkin.tm.dto.response.UserProfileResponse;
import ru.t1k.vbelkin.tm.model.Session;
import ru.t1k.vbelkin.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.t1k.vbelkin.tm.api.endpoint.IAuthEndpoint")
public class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IAuthService getAuthService() {
        return getServiceLocator().getAuthService();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    ) {
        @Nullable final String token = getAuthService().login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    ) {
        @NotNull final Session session = check(request);
        @NotNull final IAuthService authService = getServiceLocator().getAuthService();
        authService.logout(session);
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserProfileRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final User user = getServiceLocator().getUserService().findOneById(session.getUserId());
        return new UserProfileResponse(user);
    }

}
