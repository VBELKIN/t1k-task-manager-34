package ru.t1k.vbelkin.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.model.Session;
import ru.t1k.vbelkin.tm.model.User;

import javax.naming.AuthenticationException;

public interface IAuthService {

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    @SneakyThrows
    String login(@Nullable String login, @Nullable String password) throws AuthenticationException;

    @NotNull
    Session validateToken(@Nullable String token);

    void logout(@Nullable Session session);

}
