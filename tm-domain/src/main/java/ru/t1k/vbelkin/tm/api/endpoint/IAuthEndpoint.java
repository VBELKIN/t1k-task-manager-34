package ru.t1k.vbelkin.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1k.vbelkin.tm.dto.request.UserLoginRequest;
import ru.t1k.vbelkin.tm.dto.request.UserLogoutRequest;
import ru.t1k.vbelkin.tm.dto.request.UserProfileRequest;
import ru.t1k.vbelkin.tm.dto.response.UserLoginResponse;
import ru.t1k.vbelkin.tm.dto.response.UserLogoutResponse;
import ru.t1k.vbelkin.tm.dto.response.UserProfileResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IAuthEndpoint extends IEndPoint {

    @NotNull
    String NAME = "AuthEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndPoint.newInstance(connectionProvider, NAME, SPACE, PART, IAuthEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndPoint.newInstance(host, port, NAME, SPACE, PART, IAuthEndpoint.class);
    }

    @NotNull
    @WebMethod
    UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    );

    @NotNull
    @WebMethod
    UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    );

    @NotNull
    @WebMethod
    UserProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserProfileRequest request
    );

}
