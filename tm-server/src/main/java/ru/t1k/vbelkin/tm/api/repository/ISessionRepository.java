package ru.t1k.vbelkin.tm.api.repository;

import ru.t1k.vbelkin.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
