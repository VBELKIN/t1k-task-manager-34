package ru.t1k.vbelkin.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class TaskGetByIdRequest extends AbstractUserRequest {

    @Nullable
    private String taskId;

    public TaskGetByIdRequest(@Nullable String token) {
        super(token);
    }

}
