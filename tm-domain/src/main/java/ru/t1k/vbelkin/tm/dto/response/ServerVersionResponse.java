package ru.t1k.vbelkin.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ServerVersionResponse extends AbstractResponse {

    private String version;

}
