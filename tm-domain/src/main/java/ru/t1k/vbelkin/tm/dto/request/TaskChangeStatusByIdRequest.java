package ru.t1k.vbelkin.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public class TaskChangeStatusByIdRequest extends AbstractUserRequest {

    @Nullable
    private String taskId;

    @Nullable
    private Status status;

    public TaskChangeStatusByIdRequest(@Nullable String token) {
        super(token);
    }

}
