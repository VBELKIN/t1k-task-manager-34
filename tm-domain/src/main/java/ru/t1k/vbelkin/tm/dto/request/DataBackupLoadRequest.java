package ru.t1k.vbelkin.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class DataBackupLoadRequest extends AbstractUserRequest {

    public DataBackupLoadRequest(@Nullable String token) {
        super(token);
    }

}
