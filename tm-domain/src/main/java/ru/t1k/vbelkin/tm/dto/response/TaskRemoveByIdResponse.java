package ru.t1k.vbelkin.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.model.Task;

@NoArgsConstructor
public class TaskRemoveByIdResponse extends AbstractTaskResponse {

    public TaskRemoveByIdResponse(@Nullable final Task task) {
        super(task);
    }

}
